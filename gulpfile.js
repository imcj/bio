var gulp = require('gulp'),
    sass = require('gulp-sass'),
    bower = require('gulp-bower'),
    scp = require('gulp-scp2'),
    concat = require('gulp-concat'),
    env = require('gulp-env'),
    process = require('process'),
    clean = require('gulp-clean'),
    s3 = require('gulp-s3')

gulp.task('html', function() {
    return gulp.src('index.html')
        .pipe(gulp.dest('dist/'))
})

gulp.task('scss', function() {
    return gulp.src('scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('build/css/'))
})

gulp.task('bower', function() {
    return bower('./bower_components')
        .pipe(gulp.dest('./bower_components'))
})

gulp.task('env', function() {
    envs = env({
        file: ".env.json"
    })
})

gulp.task('copy-to-deploy-test', function() {
    return gulp.src('dist/index.html')
        .pipe(scp({
            host: 'imcj.me',
            privateKey: require('fs').readFileSync(process.env.privateKey),
            username: 'root',
            dest: '/var/www/bio-test'
        }))
})

gulp.task('concat-script', function() {
    return gulp.src([
        './bower_components/jquery/dist/jquery.slim.min.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./dist/public/js'))
})

gulp.task('concat-css', function() {
    return gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.min.css', 
        'build/css/app.css'])
        .pipe(concat('app.css'))
        .pipe(gulp.dest('dist/public/css/'))
})

gulp.task('clean', function() {
    return gulp.src(['dist', 'build'])
        .pipe(clean())
})

gulp.task('_deploy-to-s3', function() {
    gulp
        .src('./dist/**')
        .pipe(s3({
            "key": process.env.AWS_ACCESS_KEY_ID,
            "secret": process.env.AWS_SECRET_ACCESS_KEY,
            "bucket": process.env.AWS_S3_BUCKET,
            "region": process.env.AWS_REGION
        }));
});

gulp.task('_deploy', function() {
    var deployBucket = "";

    console.log(process.env.RUNNER)

    var nodeEnv = process.env.NODE_ENV.toLowerCase()
    if ("stage" == nodeEnv) {
        deployBucket = process.env.AWS_S3_BUCKET_STAGE
    } else if ("production" == nodeEnv) {
        deployBucket = process.env.AWS_S3_BUCKET
    } else
        deployBucket = process.env.AWS_S3_BUCKET_STAGE

    var s3Object = {
        "key": process.env.AWS_ACCESS_KEY_ID,
        "secret": process.env.AWS_SECRET_ACCESS_KEY,
        "bucket": deployBucket,
        "region": process.env.AWS_REGION
    }
    
    return gulp
        .src('./dist/**')
        .pipe(s3(s3Object));
})

gulp.task('deploy-test', ['env', 'copy-to-deploy-test'])
gulp.task('build', [ 'html', 'scss', 'bower', 'concat-script',
    'concat-css'])
gulp.task('deploy', ['env', '_deploy'])
gulp.task('default', ['build', 'clean'])