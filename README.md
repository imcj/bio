# BIO

这是CJ的BIO，用来演示一些技术，包括

- gulp
- bootstrap 4
- gitlab ci & cd
- Docker
- aws s3
- aws cloudfront

近期打算

- aws Route 53

未来可能会有的

- aws ApiGateway
- aws lambda
- aws dynamodb